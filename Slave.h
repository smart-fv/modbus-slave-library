#include "mbed.h"

class Slave {

    private:
    BufferedSerial serial;
    uint16_t* registersM;
    uint16_t* registersP;
    unsigned char frame[64];
    unsigned char slave_address;
    unsigned char size;
         
    void send();
    void processF3();
    void processF4();
    void processF6();
    uint16_t calcCRC(unsigned char bufferSize);

    public:
    Slave(PinName tx, PinName rx, long baud, unsigned char slave_address, 
        uint16_t* registersM = NULL, uint16_t* registersP = NULL);
    void cycle();
};