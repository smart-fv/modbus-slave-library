#include "Slave.h"

#pragma region PUBLIC

// Costruttore
Slave::Slave(PinName tx, PinName rx, long baud, unsigned char slave_address, uint16_t* registersM, uint16_t* registersP) : serial(tx, rx, baud) {
    serial.set_format(8, BufferedSerial::Parity::Even, 1);
    this->slave_address = slave_address;
    this->registersM = registersM;
    this->registersP = registersP;
} 

// Routine di ricezione - elaborazione - risposta
void Slave::cycle() {    
    if (serial.readable()) {        
        thread_sleep_for(50);
        serial.read((void*)frame, 8);
        if (frame[0] != slave_address) {
            return;
        }
        switch (frame[1]) {
            case 3:
                processF3();
                return;
            case 4:
                processF4();
                return;
            case 6:
                processF6();
                return;
        }
    }
    else {
        thread_sleep_for(50);
    }    
}

#pragma endregion

#pragma region PRIVATE

// Invio della risposta
void Slave::send() {
    serial.write((void*)frame, size);
    // printf("Sent frame:\r\n");
    for (int i = 0; i < size; i++) {
        // printf("%d ", frame[i]);
    }
    // printf("\r\n");
}

// Elaborazione richiesta F3
void Slave::processF3() {
    // printf("Processing F3...\n");
    uint16_t crc = (frame[6] << 8) | frame[7];
    if (crc != calcCRC(6)) {
        return;
    }
    uint16_t starting_address, n_of_registers;
    starting_address = (frame[2] << 8) | frame[3];
    n_of_registers = (frame[4]) << 8 | frame[5];
    size = 5 + (n_of_registers * 2);
    frame[2] = (unsigned char) n_of_registers * 2;
    for (int a = 0, f = 3; a < n_of_registers; a++, f+=2) {
        frame[f] = registersM[starting_address + a] >> 8;
        frame[f + 1] = registersM[starting_address + a] & 0xFF;
    }
    crc = calcCRC(size-2);
    frame[size-2] = crc >> 8;
    frame[size-1] = crc & 0xFF;
    send();
}

// Elaborazione richiesta F4
void Slave::processF4() {
    // printf("Processing F4...\n");
    uint16_t crc = (frame[6] << 8) | frame[7];
    if (crc != calcCRC(6)) {
        return;
    }
    uint16_t starting_address, n_of_registers;
    starting_address = (frame[2] << 8) | frame[3];
    n_of_registers = (frame[4]) << 8 | frame[5];
    size = 5 + (n_of_registers * 2);
    frame[2] = (unsigned char) n_of_registers * 2;
    for (int a = 0, f = 3; a < n_of_registers; a++, f+=2) {
        frame[f] = registersP[starting_address + a] >> 8;
        frame[f + 1] = registersP[starting_address + a] & 0xFF;
    }
    crc = calcCRC(size-2);
    frame[size-2] = crc >> 8;
    frame[size-1] = crc & 0xFF;
    send();
}

// Elaborazione richiesta F6
void Slave::processF6() {
    // printf("Processing F6...\n");
    // printf("Received frame:\r\n");
    for (int i = 0; i < size; i++) {
        // printf("%d ", frame[i]);
    }
    // printf("\r\n");
    uint16_t recCRC = (frame[6] << 8) | frame[7];
    if (recCRC != calcCRC(6)) {
        return;
    }
    size = 8;
    uint16_t reg_add = (frame[2] << 8) | frame[3];
    uint16_t reg_val = (frame[4] << 8) | frame[5];
    registersM[reg_add] = reg_val;
    send();
}

// Calcolo del CRC
uint16_t Slave::calcCRC(unsigned char bufferSize) {
    uint16_t temp, flag;
    temp = 0xFFFF;
    for (unsigned char i = 0; i < bufferSize; i++) {
        temp = temp ^ frame[i];
        for (unsigned char j = 1; j <= 8; j++) {
            flag = temp & 0x0001;
            temp >>= 1;
            if (flag)
                temp ^= 0xA001;
        }
    }
    uint16_t temp2, temp3;
    temp2 = temp >> 8;
    temp3 = temp & 0xFF;
    temp = (temp3 << 8) | temp2; 
    return temp;
}

#pragma endregion